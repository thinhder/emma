$("input:radio.someClass:checked");

var buttonRenovatie = document.querySelector('.renovatie-btn'),
    buttonZakelijk = document.querySelector('.zakelijk-btn'),
    buttonNieuwbouw = document.querySelector('.nieuwbouw-btn'),
    buttonRestauratie = document.querySelector('.restauratie-btn'),
    buttonAll = document.getElementById('all'),
    
    mason = document.querySelector('.mason'),
    mainDer = document.getElementById('main2'),
    
    // blockZakelijk = document.querySelectorAll('.category-zakelijk'),
    // blockRestauratie = document.querySelectorAll('.category-restauratie'),
    // blockNieuwbouw = document.querySelectorAll('.category-nieuwbouw'),
    // blockRenovatie = document.querySelectorAll('.category-renovatie'),
    
    block = document.querySelectorAll('.block'),
    blockHidden = document.querySelectorAll('.block--hidden'),

    buttonFilter = document.querySelector('.button__filter'),
    filterToggle = document.querySelector('.filter__toggle');




// __  __ ______ _   _ _    _   _______ ____   _____  _____ _      ______ 
// |  \/  |  ____| \ | | |  | | |__   __/ __ \ / ____|/ ____| |    |  ____|
// | \  / | |__  |  \| | |  | |    | | | |  | | |  __| |  __| |    | |__   
// | |\/| |  __| | . ` | |  | |    | | | |  | | | |_ | | |_ | |    |  __|  
// | |  | | |____| |\  | |__| |    | | | |__| | |__| | |__| | |____| |____ 
// |_|  |_|______|_| \_|\____/     |_|  \____/ \_____|\_____|______|______|


var toggler = document.querySelector('.toggle');

toggler.addEventListener('click', function() {
    document.body.classList.toggle("menu--opened");
});




// __  __           _____  ____  _   _    _____ _____  _____ _____  
// |  \/  |   /\    / ____|/ __ \| \ | |  / ____|  __ \|_   _|  __ \ 
// | \  / |  /  \  | (___ | |  | |  \| | | |  __| |__) | | | | |  | |
// | |\/| | / /\ \  \___ \| |  | | . ` | | | |_ |  _  /  | | | |  | |
// | |  | |/ ____ \ ____) | |__| | |\  | | |__| | | \ \ _| |_| |__| |
// |_|  |_/_/    \_\_____/ \____/|_| \_|  \_____|_|  \_\_____|_____/ 


if(mason != null) {
  function refresh() {
    if (mason.offsetWidth > 1005) {
      //console.log('groter dan 1079');
      for (k = 0; k < block.length; k += 8) {
        block[k].classList.add('first');
      }
    } else if (mason.offsetWidth > 840) {
      //console.log('groter dan 840');
      for (k = 0; k < block.length; k += 6) {
        block[k].classList.add('first');
      }
    } else if (mason.offsetWidth > 720) {
      //console.log('groter dan 840');
      for (k = 0; k < block.length; k += 6) {
        block[k].classList.add('first');
      }
    } else if (mason.offsetWidth > 496) {
      //console.log('groter dan 840');
      for (k = 0; k < block.length; k += 8) {
        block[k].classList.add('first');
      }
    }
  }

  refresh();


  function recalculate() {
    console.log('recalculating');

    var block = document.querySelectorAll('.block');
    if (mason.offsetWidth >= 1005) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 8) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 900) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 7) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 800) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 6) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 784) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 6) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 769) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 5) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 720) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 6) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 620) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 5) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 541) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 4) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 520) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 6) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 496) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 6) {
          block[k].classList.add('first');
        }
    } else if (mason.offsetWidth >= 360) {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 4) {
          block[k].classList.add('first');
        }
    } else {
        for (j = 0; j < block.length; j++) {
          block[j].classList.remove('first');
        }
        for (k = 0; k < block.length; k += 3) {
          block[k].classList.add('first');
        }
    }

  }

  recalculate()
}




//  ______ _____ _   _______ ______ _____            _    _           _____ _    _ 
// |  ____|_   _| | |__   __|  ____|  __ \     _    | |  | |   /\    / ____| |  | |
// | |__    | | | |    | |  | |__  | |__) |  _| |_  | |__| |  /  \  | (___ | |__| |
// |  __|   | | | |    | |  |  __| |  _  /  |_   _| |  __  | / /\ \  \___ \|  __  |
// | |     _| |_| |____| |  | |____| | \ \    |_|   | |  | |/ ____ \ ____) | |  | |
// |_|    |_____|______|_|  |______|_|  \_\         |_|  |_/_/    \_\_____/|_|  |_|


if(filterToggle != null) {
    buttonFilter.addEventListener('click', function(){
        buttonFilter.classList.toggle('buttonfilter--opened');
        filterToggle.classList.toggle('filter__toggle--opened');
    });
}

if(buttonAll != null) {
  buttonAll.addEventListener('click', function(event) {
    history.pushState("", document.title, window.location.pathname + window.location.search);
    event.stopPropagation();
  });

  buttonRenovatie.addEventListener('click', function(event) {
    window.location.hash = "renovatie";
    event.stopPropagation();
  });

  buttonZakelijk.addEventListener('click', function(event) {
    window.location.hash = "zakelijk";
    event.stopPropagation();
  });

  buttonNieuwbouw.addEventListener('click', function(event) {
    window.location.hash = "nieuwbouw";
    event.stopPropagation();
  });

  buttonRestauratie.addEventListener('click', function(event) {
    window.location.hash = "restauratie";
    event.stopPropagation();
  });

  window.onresize = function(event) {
      console.log(mason.offsetWidth);
      recalculate();
  };
}

var urlHash = window.location.hash;

if (urlHash.slice(1) == "restauratie") {
  console.log('restauratie');

  $('.btn').removeClass('active');
  $('#category-restauratie').attr("checked", true);

  if ( $('.category-restauratie').length ) {
      $cat = "category-restauratie";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  } else {
      $cat = "category-restauratie-en";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  }
  recalculate();


} else if (urlHash.slice(1) == "renovatie") {
  console.log('renovatie');

  $('.btn').removeClass('active');
  $('#category-renovatie').attr("checked", true);


  if ( $('.category-restauratie').length ) {
      $cat = "category-renovatie";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  } else {
      $cat = "category-renovatie-en";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  }
  recalculate();


} else if (urlHash.slice(1) == "nieuwbouw") {
  console.log('nieuwbouw');

  $('.btn').removeClass('active');
  $('#category-nieuwbouw').attr("checked", true);

  if ( $('.category-nieuwbouw').length ) {
      $cat = "category-nieuwbouw";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  } else {
      $cat = "category-nieuwbouw-en";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  }
  recalculate();

  
} else if (urlHash.slice(1) == "zakelijk") {
  console.log('zakelijk');

  $('.btn').removeClass('active');
  $('#category-zakelijk').attr("checked", true);

  if ( $('.category-zakelijk').length ) {
      $cat = "category-zakelijk";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  } else {
      $cat = "category-zakelijk-en";
      var $el = $('.' + $cat).removeClass('lati');
      var $el = $('.' + $cat).addClass('block');
      $('#parent > a').not($el).addClass('lati');
      $('#parent > a').not($el).removeClass('block');
  }
  recalculate();
} 

var menuProjecten = document.querySelector('.menu__projecten > a'),
    filterTog = document.querySelector('.filter__toggle');

menuProjecten.addEventListener('click', function(event){
    console.log('filter__toggled'); 
    menuProjecten.classList.toggle('filter__toggled--opened');
    filterTog.classList.toggle('filter__toggled');
    event.preventDefault();
});
                                                                              
$( document ).ready(function() {

  var $btns = $('.btn').click(function() {
    if (this.id == 'all') {

      $('#parent > a').removeClass('lati');
      $('#parent > a').addClass('block');
      recalculate();
    } else {

      if ( $('.category-zakelijk-en').length || $('.category-nieuwbouw-en').length || $('.category-restauratie-en').length || $('.category-renovatie-en').length) {
        var $el = $('.' + this.id + '-en').removeClass('lati');
        var $el = $('.' + this.id + '-en').addClass('block');
        $('#parent > a').not($el).addClass('lati');
        $('#parent > a').not($el).removeClass('block');
        recalculate();
      } else {
        var $el = $('.' + this.id).removeClass('lati');
        var $el = $('.' + this.id).addClass('block');
        $('#parent > a').not($el).addClass('lati');
        $('#parent > a').not($el).removeClass('block');
        recalculate();
      }

      
    }
    $btns.removeClass('active');
    $(this).addClass('active');
  });




//   _____ _      _____ _____  ______ _____  
//  / ____| |    |_   _|  __ \|  ____|  __ \ 
// | (___ | |      | | | |  | | |__  | |__) |
//  \___ \| |      | | | |  | |  __| |  _  / 
//  ____) | |____ _| |_| |__| | |____| | \ \ 
// |_____/|______|_____|_____/|______|_|  \_\
                                           

  var $status = $('.slidercounter');
  var $slickElement = $('.slider');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
      //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
      var i = (currentSlide ? currentSlide : 0) + 1;
      $status.text(i + '/' + slick.slideCount);
  });


  $('.slider').slick({
    // slidesToShow: 1,
    // slidesToScroll: 1,
    // centerPadding: '75px',
    arrows: true,
    // fade: false,
    // infinite: true,
    // adaptiveHeight: false,
    dots: false,
    // variableWidth: false,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    autoplay: false,
    // dots: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });


   $('a[data-slide]').click(function(e) {
     e.preventDefault();
     var slideno = $(this).data('slide');
     $('.slider').slick('slickGoTo', slideno - 1);
   });

  // function event,slick and index
  // version 1.5+ uses slick-current stead of slick-active
  $('.slider').on('afterChange', function(event,slick,i){
    $('.slider-nav-thumbnails a').removeClass('slick-current');
    $('.slider-nav-thumbnails a').eq(i).addClass('slick-current');             
  });

  // remember document ready on this
  $('.slider-nav-thumbnails a').eq(0).addClass('slick-current'); 

});


var slider = document.querySelector('.slider');

if (slider != null) {
  slider.addEventListener('click', function() {
      document.body.classList.toggle("slider__fullscreen");
      $('.slider').slick('setPosition');
  });

}

var button360 = document.querySelector('.sidebar__top--360'),
    buttonAutodesk = document.querySelector('.sidebar__top--autodesk'),
    backgroundAutodesk = document.querySelector('.iframe-wrap__bg'),
    background360 = document.querySelector('.panorama__bg');

if (button360 != null) {
  button360.addEventListener('click', function() {
    document.body.classList.add("opened__360");
  });

  background360.addEventListener('click', function() {
    document.body.classList.remove("opened__360");
  });

  document.onkeydown = function(e){ 
     if (window.event.keyCode == 27) {
        document.body.classList.remove("opened__360");
     }
  };
}

if (buttonAutodesk != null) {
  buttonAutodesk.addEventListener('click', function() {
    document.body.classList.add("opened__autodesk");
  });

  backgroundAutodesk.addEventListener('click', function() {
    document.body.classList.remove("opened__autodesk");
  });
}

$(document).ready(function() {
  $(".sidebar__top--autodesk").click(function() {
    var iframe = $("#autodesk__iframe");
    iframe.attr("src", iframe.data("src"));
  });
});




//  _______ _____            _   _  _____ _____ _______ _____ ____  _   _  _____ 
// |__   __|  __ \     /\   | \ | |/ ____|_   _|__   __|_   _/ __ \| \ | |/ ____|
//    | |  | |__) |   /  \  |  \| | (___   | |    | |    | || |  | |  \| | (___  
//    | |  |  _  /   / /\ \ | . ` |\___ \  | |    | |    | || |  | | . ` |\___ \ 
//    | |  | | \ \  / ____ \| |\  |____) |_| |_   | |   _| || |__| | |\  |____) |
//    |_|  |_|  \_\/_/    \_\_| \_|_____/|_____|  |_|  |_____\____/|_| \_|_____/ 
                                                                               

if (mainDer != null) {

  if (mainDer.offsetWidth > 541) {
    (function($) {

      function addBlacklistClass() {
        $('a').each(function() {
          if (this.href.indexOf('/wp-admin/') !== -1 ||
            this.href.indexOf('/wp-login.php') !== -1) {
            $(this).addClass('wp-link');
          }
        });
      }

      $(function() {

        addBlacklistClass();

        var settings = {
          anchors: 'a',
          blacklist: '.wp-link',
          onStart: {
            duration: 500,
            render: function($container) {
              $container.addClass('slide-out-new');
            }
          },
          onAfter: function($container) {

            addBlacklistClass();

            var $hash = $(window.location.hash);

            if ($hash.length !== 0) {

              var offsetTop = $hash.offset().top;

              
            }

            $container.removeClass('slide-out-new');
          }
        };

        $('#main').smoothState(settings);
      });

    })(jQuery);

  }




  if (mainDer.offsetWidth < 540) {

    var overOns = document.querySelector('.overons');

    if (overOns != null) {

      jQuery(function(){
          'use strict';
          jQuery(document).ready(function () {
              var $ = 'jQuery',
                  $body = jQuery('body'),
                  $main = jQuery('#main'),
                  $site = jQuery('html, body'),
                  smoothState;
              smoothState = $main.smoothState ({

                      debug: true,
                      prefetch: true,
                      cacheLength: 10,
                      onBefore: function($anchor, $container) {
                          // $(.overons).addClass('downdown')
                      },
                      onStart: {
                          duration: 0,
                          render: function ($container) {
                              console.log('1');
                              jQuery('#tempWrapper').remove(); // If we have the temp wrapper, kill it now.
                              $site.animate({ scrollTop: "0px" });
                              //$site.addClass('is-exiting');

                              // Make a duplicate container for animation
                              var $newContainer = $container.clone();
                              $newContainer.attr("id", "tempWrapper");
                              $newContainer.css({position:'absolute', top:$container.offset().top, width:$container.css("width")});
                              $container.css({height:$container.css("height")});
                              $container.empty(); // Empty the old content so that it takes up 0 space
                              $container.before($newContainer); // Immediately add the duplicate back on
                              jQuery('.sceneElement').removeClass('sceneElement--fadeinright'); // Just in case we have the class still
                              var element = jQuery('#tempWrapper', $newContainer);
                              setTimeout(callAnimation(element, true), 0); // Start the animation
                          }
                      },
                      onReady: {
                          duration: 0,
                          render: function ($container, $newContent) {
                              // Inject the new content
                              $container.html($newContent);

                              // do animations
                              var element = document.getElementById($container[0].id).getElementsByClassName('sceneElement')[0];
                              callAnimation(element);
                          }
                      },
                      onAfter: function ($container) {
                          location.reload();
                      },
              }).data('smoothState');
          });


      }(jQuery));

      function callAnimation(element, exiting) {
          if (!exiting) {
              jQuery(element).addClass("sceneElement--fadeinright");
              setTimeout( function(){ 
                $('#tempWrapper').remove();
              }  , 1500 );
              
          } else {
              jQuery(element).addClass('is-exiting');

          }
      }
    }


    var singleMed = document.querySelector('.single-medewerker');

    if (singleMed != null) {


    jQuery(function(){
        'use strict';
        jQuery(document).ready(function () {
            var $ = 'jQuery',
                $body2 = jQuery('body'),
                $main2 = jQuery('#main2'),
                $site2 = jQuery('html, body'),
                smoothState;
            smoothState = $main2.smoothState ({

                    debug: true,
                    prefetch: true,
                    cacheLength: 10,
                    onStart: {
                        duration: 0,
                        render: function ($container) {
                            console.log('2');
                            jQuery('#tempWrapper').remove(); // If we have the temp wrapper, kill it now.
                            $site2.animate({ scrollTop: "0px" });
                            //$site.addClass('is-exiting');

                            // Make a duplicate container for animation
                            var $newContainer = $container.clone();
                            $newContainer.attr("id", "tempWrapper");
                            //$newContainer.css({position:'absolute', top:$container.offset().top, width:$container.css("width")});
                            $container.css({height:$container.css("height")});
                            $container.empty(); // Empty the old content so that it takes up 0 space
                            $container.before($newContainer); // Immediately add the duplicate back on
                            jQuery('.sceneElement2').removeClass('sceneElement--fadeinright2'); // Just in case we have the class still
                            var element = jQuery('#tempWrapper', $newContainer);
                            setTimeout(callAnimation2(element, true), 0); // Start the animation
                        }
                    },
                    onReady: {
                        duration: 0,
                        render: function ($container, $newContent) {
                            // Inject the new content
                            $container.html($newContent);

                            // do animations
                            var element = document.getElementById($container[0].id).getElementsByClassName('sceneElement2')[0];
                            callAnimation2(element);
                        }
                    },
                    onAfter: function ($container) {
                        location.reload();
                    },
            }).data('smoothState');
        });


    }(jQuery));

    function callAnimation2(element, exiting) {
        if (!exiting) {
            jQuery(element).addClass("sceneElement--fadeinright2");
            setTimeout( function(){ 
              $('#tempWrapper').remove();
            }  , 1500 );
            
        } else {
            jQuery(element).addClass('is-exiting');

        }
    }

    }

  }
}

// var nodes = document.getElementsByTagName("IMG");
// var images = Array.prototype.slice.call(nodes);
// images.forEach(function(img) {
//   if (img.width > img.height) {
//     img.className += img.className ? ' landscape' : 'landscape';
//   }
//   else {
//     img.className += img.className ? ' portrait' : 'portrait';
//   }
// });


