<?php /* Template Name: Over ons */
  get_header(); ?>
<div id="main" class="m-scene over__ons">
<div <?php body_class(); ?>>
<div id="main2" class="m-scene2">
	<div class="overons sceneElement">
		<?php include 'grid-medewerkers.php';?>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="container center padding">
				<?php the_title( '<h2>', '</h2>' ); ?>
				<?php the_field('subtitel'); ?>
				<div class="overons__tekst">
					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile; else : endif; ?>
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>