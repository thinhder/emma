<?php get_header(); ?>
<div id="main" class="m-scene ontop">
<div <?php body_class(); ?>>
<div id="main2" class="m-scene2">
	<div class="container center single-medewerker tupac sceneElement">
		<div class="col-6 col-t-12 left single-medewerker__pic padding padding-m-0" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)">
			<a href="<?php bloginfo('url'); ?>/over-ons" data-target="1">
				<svg xmlns="http://www.w3.org/2000/svg" class="single-medewerker__pic--close" viewBox="11196 -718 20 20">
				  <defs>
				    <style>
				      .cls-1 {
				        fill: #fff;
				        fill-rule: evenodd;
				      }
				    </style>
				  </defs>
				  <path id="Path_89" data-name="Path 89" class="cls-1" d="M10.98,9.982l8.806,8.806a.706.706,0,0,1-1,1L9.982,10.98l-8.77,8.806a.689.689,0,0,1-1,0,.689.689,0,0,1,0-1l8.77-8.806L.214,1.212a.689.689,0,0,1,0-1,.689.689,0,0,1,1,0l8.77,8.77L18.788.214a.689.689,0,0,1,1,0,.689.689,0,0,1,0,1Z" transform="translate(11196 -718)"/>
				</svg>
			</a>
		</div>
		<div class="single__bio col-6 col-t-12 left">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="padding">
				<?php the_title( '<h3>', '</h3>' ); ?>
				<div class="single__bio--functie"><?php the_field('functie') ?></div>
				<?php the_content(); ?>
				<div class="medewerkers__social">
					<?php if( get_field('linkedin') ): ?>
					<a href="<?php the_field('linkedin') ?>" target="_blank">
						<svg viewBox="5683 1284 21.672 21.672">
						  <defs>
						    <style>
						      .cls-1 {
						        fill: #e36c41;
						        fill-rule: evenodd;
						      }
						    </style>
						  </defs>
						  <path id="Path_88" data-name="Path 88" class="cls-1" d="M19.621,21.672H2.051A2.065,2.065,0,0,1,0,19.621V2.051A2.065,2.065,0,0,1,2.051,0H19.582a2.073,2.073,0,0,1,2.09,2.051V19.582A2.073,2.073,0,0,1,19.621,21.672ZM5.341,4.644A1.443,1.443,0,0,0,3.87,6.192,1.457,1.457,0,0,0,5.3,7.74h0A1.465,1.465,0,0,0,6.772,6.192,1.414,1.414,0,0,0,5.341,4.644ZM6.966,8.669H4.025V17.8H6.966Zm11.61,4.063c0-2.709-1.393-4.218-3.406-4.218a3.035,3.035,0,0,0-2.748,1.471V8.746H9.288c.039.813,0,9.056,0,9.056h3.135V12.887a1.74,1.74,0,0,1,.116-.7,1.672,1.672,0,0,1,1.548-1.084c1.084,0,1.432.813,1.432,2.012V17.8h3.057Zm-6.153-2.709Z" transform="translate(5683 1284)"/>
						</svg>
					</a>
					<?php endif; ?>

					<?php if( get_field('twitter') ): ?>
					<a href="<?php the_field('twitter') ?>" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="5720.241 1284 21.672 21.672">
						  <defs>
						    <style>
						      .cls-1 {
						        fill: #e36c41;
						        fill-rule: evenodd;
						      }
						    </style>
						  </defs>
						  <path id="Path_89" data-name="Path 89" class="cls-1" d="M21.672,2.5l-2.032,2.5v.416A8.865,8.865,0,0,1,19.5,6.737,16.125,16.125,0,0,1,14.9,18.3C7.721,25.368,0,19.13,0,19.13c6.1,0,6.1-2.5,6.1-2.5-1.354,0-4.063-3.327-4.063-3.327.677.832,2.032,0,2.032,0C.677,10.771.677,8.318.677,8.318c.677.832,2.032,0,2.032,0A6.686,6.686,0,0,1,1.354.832c.677,4.159,9.481,5.822,9.481,5.822l.1-.083a12.381,12.381,0,0,1-.1-1.58C10.836,2,12.8,0,15.238,0a4.061,4.061,0,0,1,3.318,1.913l.406-.25L20.995,0,19.64,3.327Z" transform="translate(5720.241 1284)"/>
						</svg>
					</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; else : endif; ?>
		</div>
	</div>
	<div class="col-m-0">
	<?php include 'grid-medewerkers.php';?>
	</div>

	<?php $the_query = new WP_Query( 
		array(
	        'post_type' => 'page',
	        'meta_key' => '_wp_page_template',
	        'meta_value' => 'page-overons.php'
	    )
	); ?>

	<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>

		<div class="container center padding col-m-0">
			<?php the_title( '<h2>', '</h2>' ); ?>
			<?php the_field('subtitel'); ?>
			<div class="overons__tekst">
				<?php the_content(); ?>
			</div>
		</div>

     <?php endwhile;?>
</div>
</div>
</div>
<?php get_footer(); ?>
