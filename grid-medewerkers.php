<div class="container center">
	<div class="projecten-grid">
		
		<div class="ruler"></div>
		<div class="ruler1"></div>
		<div class="ruler2"></div>
		<div class="ruler3"></div>
		<div class="ruler4"></div>
		<div class="ruler5"></div>
		<div class="ruler6"></div>
		<div class="ruler7"></div>
		<div class="ruler8"></div>
		<div class="ruler9"></div>
		<div class="ruler10"></div>
		<div class="ruler11"></div>
		<div class="ruler12"></div>

		<div class="mason__grid overons__grid">
			<?php 
		        $query_default = new WP_Query( array(
		            'order'        => 'ASC',
		            'post_type'    => 'Medewerkers'
		        ) );

	            if ( $query_default->have_posts() ) :

	                while ( $query_default->have_posts() ) : $query_default->the_post();?> 
					<a href="<?php echo get_permalink(); ?>" data-target="0" class="block" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)">
			            <div >
			            	
				                <div class="block-content">
				                	<div class="block-content__name">
					                	<h2><?php the_title(); ?></h2>
					                	<h3><?php the_field('functie'); ?></h3>
					                </div>
				                </div>
				           
			            </div>
 					</a>
	            <?php
	                    
	            	endwhile;

				endif; 

	            wp_reset_query();
	    	?>

		</div>
	</div>
</div>