<?php get_header(); ?>
<script type="text/javascript" src="https://cdn.pannellum.org/2.3/pannellum.js"></script>
<div class="container center single-project">


	<div class="col-8 col-t-12 left padding single-project__slider">

		<ul class="slider">

			<?php 

			$images = get_field('fotos');

			if( $images ): ?>
			    
			        <?php foreach( $images as $image ): ?>
			            <li data-thumb="<?php echo $image['sizes']['thumbnail']; ?>">
			            	<div class="fscenter">
				                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
				                <p><?php echo $image['title'] ?>
				                <?php $thinh = $image['caption'] ?>
								
				                <?php if(!empty($thinh)){
								  echo $thinh . '</p>';
								  } ?>
							</div>
			            </li>
			        <?php endforeach; ?>
			    
			<?php endif; ?>

			<?php 

			$tekeningen = get_field('tekeningen');

			if( $tekeningen ): ?>
			    
			        <?php foreach( $tekeningen as $tekening ): ?>
			            <li class="slider__tekening">
			            	<div class="fscenter">
			                <img src="<?php echo $tekening['sizes']['large']; ?>" alt="<?php echo $tekening['alt']; ?>" />
			                <p><?php echo $tekening['title'] ?> <?php echo $tekening['caption'] ?></p>
			              	</div>  
			            </li>
			        <?php endforeach; ?>
			    
			<?php endif; ?>


		</ul>
			<div class="prevnextbutton prev">
				<div class="prev__icon">
					<svg version="1.1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve">
						<g>
							<path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
								c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/>
						</g>
					</svg>
				</div>
			</div>
			<div class="prevnextbutton next">
				<div class="next__icon">
					<svg version="1.1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve">
						<g>
							<path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
								c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/>
						</g>
					</svg>
				</div>
			</div>
		<span class="slidercounter"></span>
	</div>

	<div class="slider-thumbnails col-4 col-t-0 left padding">
		
			<?php 

			$images = get_field('fotos');
			$tekeningen = get_field('tekeningen');

			if( $images ): $i = 1;?>
			    <div class="slider-nav-thumbnails">
			        <?php foreach( $images as $image ): ?>
			            <a href="#" data-slide="<?php echo $i++ ?>">
			                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			            </a>
			        <?php endforeach; ?>

			    </div>
				<div class="slider-nav-thumbnails">
				
			    	<?php foreach( $tekeningen as $tekening ): ?>
			            <a href="#" data-slide="<?php echo $i++ ?>">
			                <img src="<?php echo $tekening['sizes']['thumbnail']; ?>" alt="<?php echo $tekening['alt']; ?>" />
			            </a>
			        <?php endforeach; ?>
			    </div>
			    
			<?php endif; ?>
		
	</div>
	<div class="clearfix"></div>
	
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="container center">
			<div class="sidebar__top col-4 right col-t-12 left-t padding">
				<ul>
					<?php $file = get_field('pdf');
							if( $file ): ?>
						<li class="sidebar__top--pdf">

							<a href="<?php echo $file['url']; ?>" download="FileName"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon--pdf.svg"> DOWNLOAD .PDF</a>
						</li>
					<?php endif; ?>
					<?php if( get_field('360_foto') ): ?>
						<li class="sidebar__top--360">
							<span><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon--360.svg"> 360° VIEW</span>
						</li>
					<?php endif; ?>
					<?php if( get_field('autodesk360') ): ?>
						<li class="sidebar__top--autodesk">
							<span><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon--cube.svg"> AUTODESK360</span>
						</li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="project-main col-8 col-t-12 padding left">
				<?php the_title( '<h1 class="single-project--titel">', '</h1>' ); ?>
				<h2 class="single-project--subtitel"><?php the_field('subtitel'); ?></h2>
				<?php the_content(); ?>
				
				<?php 	$category = get_the_category(); 
						$firstCategory = $category[0]->cat_name; 
						$firstCategory = strtolower($firstCategory);
				?>

			</div>
			
			<div class="single-project__sidebar col-4 col-t-12 padding left">
				
				<div class="row">
					<div class="col-t-6 left padding">
						<?php if( get_field('opdracht') ): ?>

							<h3><?php _e('Opdracht', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('opdracht'); ?></p>
						<?php endif; ?>
						
						<?php if( get_field('opdrachtgever') ): ?>
							<h3><?php _e('Opdrachtgever', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('opdrachtgever'); ?></p>
						<?php endif; ?>

						<?php if( get_field('ontwerp') ): ?>
							<h3><?php _e('Ontwerp', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('ontwerp'); ?></p>
						<?php endif; ?>

						<?php if( get_field('ontwerp') ): ?>
							<h3><?php _e('Status', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('status'); ?></p>
						<?php endif; ?>

						<?php if( get_field('team_emma') ): ?>
							<h3><?php _e('Team Emma', 'emma'); ?></h3>
							<ul>
								<?php
								if( have_rows('team_emma') ):
								    while ( have_rows('team_emma') ) : the_row(); ?>

								        <li><?php the_sub_field('naam'); ?></li>

								<?php endwhile;

								else :

								endif;

								?>
							</ul>
						<?php endif; ?>
					</div>
					<div class="col-t-6 left padding">	
						<?php if( get_field('adviseur_constructie') ): ?>
							<h3><?php _e('Adviseur constructie', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('adviseur_constructie'); ?></p>
						<?php endif; ?>

						<?php if( get_field('adviseur_installaties') ): ?>
							<h3><?php _e('Adviseur installaties', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('adviseur_installaties'); ?></p>
						<?php endif; ?>

						<?php if( get_field('restauratiearchitect') ): ?>
							<h3><?php _e('Restauratiearchitect', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('restauratiearchitect'); ?></p>
						<?php endif; ?>

						<?php if( get_field('adviseur_landschap') ): ?>
							<h3><?php _e('Adviseur landschap', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('adviseur_landschap'); ?></p>
						<?php endif; ?>

						<?php if( get_field('hoofdaannemer') ): ?>
							<h3><?php _e('Hoofdaannemer', 'emma'); ?></h3>
							<p class="sidebar--p"><?php the_field('hoofdaannemer'); ?></p>
						<?php endif; ?>
					</div>
				</div>

			</div>

			<?php if( get_field('360_foto') ): ?>
				<?php 

				$image = get_field('360_foto'); 
				if( !empty($image) ): ?>

				<div class="panorama__wrapper">
					<div id="panorama"></div>
				</div>
				<div class="panorama__bg"></div>
				<script>pannellum.viewer('panorama', {
				    "type": "equirectangular",
				    "compass": true,
				    "autoLoad": false,
				    "autoRotate": -10,
				    "hfov": 160,
				    "autoRotateInactivityDelay": 3000,
				    "maxLevel": 16,
				    "panorama": "<?php echo $image['sizes']['pano']; ?>"
				});</script>

				<?php endif; ?>
			<?php endif; ?>
		</div>

		<a class="single__vergelijkbare" href="<?php echo esc_url( home_url( '/' ) ); ?>#<?php echo $firstCategory ?>"><div>+ <?php _e('Vergelijkbare projecten', 'emma'); ?></div></a>
	<?php endwhile; else : endif; ?>

	<?php if( get_field('autodesk360') ): ?>
	<div class="iframe-wrap">
		<iframe id="autodesk__iframe" src="about:blank" data-src="<?php the_field('autodesk360') ?>" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
	</div>
	<div class="iframe-wrap__bg"></div>
	<?php endif; ?>

</div>



<?php get_footer(); ?>

