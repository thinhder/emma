<!doctype html>

<html>
<head>
	<title><?php bloginfo('title' ); ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
	<link rel="stylesheet" href="https://cdn.pannellum.org/2.3/pannellum.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css">

	<?php wp_head(); ?>
</head>
<body <?php body_class( 'class-name' ); ?>>
	<header>
		<nav class="container center padding">
			<div class="logo left">
				<a href="<?php echo bloginfo('url') ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="emma logo">
				</a>
			</div>
			<div class="toggle">
				<div class="hamburger">
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
                <div class="closed stuntro">
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
			</div>
			<div class="nav__wrapper">

				<div class="lang right"><?php pll_the_languages( array( 'display_names_as' => 'slug' ) );?></div>
					<?php
					wp_nav_menu( array( 
					    'theme_location' => 'primary', 
					    'container_class' => 'navmenu padding right' ) ); 
					?> 

			</div>
		</nav>
		<div class="filters__container container center">
			<div class="filters">
				<?php 
				if ( is_page_template( 'page-projecten.php' ) ) { ?>
					<div class="btn button__filter">Filter</div>
					<form class="filter__toggle">
					<ul>
						<li>
							<label class="control control--radio">All
								<input type="radio" name="filter" value="a" class="btn active" id="all" checked="checked"> 
							<div class="control__indicator"></div>
							</label>
						</li>

						<div class="filter__toggle--kwad">

						<li>
							<label class="control control--radio label--renovatie"><?php _e('Renovatie', 'emma'); ?>
								<input type="radio" name="filter" value="b" class="btn renovatie-btn" id="category-renovatie"> 
							<div class="control__indicator"></div>
							</label>
						</li>
						
						<li>
							<label class="control control--radio label--nieuwbouw"><?php _e('Nieuwbouw', 'emma'); ?>
								<input type="radio" name="filter" value="c" class="btn nieuwbouw-btn" id="category-nieuwbouw"> 
							<div class="control__indicator"></div>
							</label>
						</li>
						
						<li>
							<label class="control control--radio label--restauratie"><?php _e('Restauratie', 'emma'); ?>
								<input type="radio" name="filter" value="d" class="btn restauratie-btn" id="category-restauratie"> 
							<div class="control__indicator"></div>
							</label>
						</li>
						
						<li>
							<label class="control control--radio label--zakelijk"><?php _e('Zakelijk', 'emma'); ?>
								<input type="radio" name="filter" value="e" class="btn zakelijk-btn" id="category-zakelijk"> 
							<div class="control__indicator"></div>
							</label>
						</li>

						</div>
					</ul>

					</form>

					<style>
						@media (min-width: 701px) {
						    .menu__projecten a:before {
						        position: relative;
						        content: '+';
						        right: 5px;
						    }

						    .filter__toggled--opened:before {
						        content: '-' !important;
						    }
						}
					    .button__filter:before {
					        position: relative;
					        content: '+';
					        right: 5px;
					    }
					    .buttonfilter--opened:before {
					        content: '-' !important;
					    }
					    @media (max-width: 700px) {
						    .filters {
						    	margin-top: 10px;
						    	margin-bottom: 40px;
						    }
						}
						.filters__container {
							display: block;
						}
					</style>
				<?php } else {
				    
				} ?>
			</div>
		</div>
	</header>