<?php /* Template Name: Projecten */ get_header(); ?>

<div class="container center">
	<div class="mason projecten-grid">

		<div class="ruler"></div>
		<div class="ruler1"></div>
		<div class="ruler2"></div>
		<div class="ruler3"></div>
		<div class="ruler4"></div>
		<div class="ruler5"></div>
		<div class="ruler6"></div>
		<div class="ruler7"></div>
		<div class="ruler8"></div>
		<div class="ruler9"></div>
		<div class="ruler10"></div>
		<div class="ruler11"></div>
		<div class="ruler12"></div>

		<div class="mason__grid" id="parent">
			<?php 
		        // $query_default = new WP_Query( array(
		        //     'orderby'      => 'rand',
		        //     'post_type'    => 'Projecten',
		        //     'posts_per_page' => -1
		        // ) );

		        $args = [ 
				    'posts_per_page'      => -1, 
				    'orderby'             => 'rand', 
				    'post_type'           => 'Projecten', 
				    'ignore_sticky_posts' => true,
				];

				add_filter( 'posts_orderby', 'force_random_day_seed' );
				$q = new WP_Query( $args );
				remove_filter( 'posts_orderby', 'force_random_day_seed' );

	            if ( $q->have_posts() ) :

	                while ( $q->have_posts() ) : $q->the_post();?> 
	            


			            	<a href="<?php echo get_permalink(); ?>" <?php post_class( 'block' ); ?> style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)">
				                <div class="block-content">
				                	<h2><?php the_title(); ?></h2>
				                </div>
				            </a>


	            <?php
	                    
	            	endwhile;

				endif; 

	            wp_reset_query();
	    	?>
    	</div>
	</div>

</div>

<?php get_footer(); ?>