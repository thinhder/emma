	<footer>
		<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script defer src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
		<script defer src="<?php echo get_stylesheet_directory_uri(); ?>/js/smoothstate.js"></script>
		
		<script defer src="https://cdn.pannellum.org/2.3/pannellum.js"></script>

		<script defer src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
	</footer>

<?php wp_footer(); ?>

</body>
</html>